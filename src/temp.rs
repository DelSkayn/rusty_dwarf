#[derive(Debug,Deserialize)]
#[serde(rename = "region")]
pub struct UndergroundRegion {
    pub id: i64,
    #[serde(rename = "type")]
    pub ty: String,
    pub depth: i64,
}

#[derive(Debug,Deserialize)]
#[serde(rename = "site")]
pub struct Site {
    pub id: i64,
    #[serde(rename = "type")]
    pub ty: String,
    pub name: String,
    pub coords: String,
    pub rectangle: String,
    pub structures: Vec<()>,
}

#[derive(Debug,Deserialize)]
#[serde(rename = "artifact")]
pub struct Artifact {
    pub id: i64,
    pub name: Option<String>,
    pub item: String,
}

#[derive(Debug,Deserialize)]
#[serde(rename = "historical_figure")]
pub struct HistorigalFigure{
    pub id: i64,
    pub name: String,
    pub race: String,
    pub caste: String,
    pub appeared: i64,
    pub birth_year: i64,
    pub birth_seconds72: i64,
    pub death_year: i64,
    pub death_seconds72: i64,
    pub associated_type: String,
    pub hf_link: Vec<HFLink>,
    pub entity_link: Vec<EntityLink>,
    pub sphere: Vec<String>,
}

#[derive(Debug,Deserialize)]
#[serde(rename = "entity_link")]
pub struct EntityLink{
    pub link_type: String,
    pub entity_id: i64,
    pub link_strenght: Option<i64>,
}

#[derive(Debug,Deserialize)]
#[serde(rename = "hf_link")]
pub struct HFLink{
    pub link_type: String,
    pub hfid: i64,
    pub link_strenght: Option<i64>,
}

#[derive(Debug,Deserialize)]
#[serde(rename = "entity_former_position_link")]
pub struct FormerPositionLink{
    pub position_profile_id: i64,
    pub entity_id: i64,
    pub start_year: i64,
    pub end_uear: i64,
}
