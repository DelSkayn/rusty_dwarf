#![allow(unused_imports)]

extern crate quick_xml;
#[macro_use]
extern crate log;
#[macro_use]
extern crate serde;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate quick_error;
#[macro_use]
extern crate juniper;
#[macro_use]
extern crate juniper_codegen;

pub mod model;
pub use self::model::DfWorld;

pub mod read;
use self::read::{Read,Value};

pub mod error;
pub use self::error::Error;


use std::{env};
use std::io::{BufRead};
use std::path::Path;
use std::str::FromStr;


pub trait Parse<R: BufRead>: Sized{
    fn parse(r: &mut Read<R>, buf: &mut Vec<u8>) -> Result<Self,Error>;
}

pub fn parse_legends<P: AsRef<Path>>(path: P) -> Result<DfWorld,Error>{
    let mut buf = Vec::new();
    let mut r = Read::from_file(path)?;
    while !r.next(&mut buf)?.is_start_name("df_world",&r) {}
    DfWorld::parse(&mut r,&mut buf)
}



