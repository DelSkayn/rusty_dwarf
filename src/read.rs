
use std::io::{BufRead,BufReader};
use std::fs::File;
use std::path::Path;
use std::str::FromStr;
use std::borrow::Cow;

use quick_xml::Reader;
use quick_xml::events::Event;

use super::{Error,Parse};

pub struct Read<R: BufRead + 'static>{
    reader: Reader<R>,
    peek: Option<Value<'static>>
}

impl Read<BufReader<File>>{
    pub fn from_file<P: AsRef<Path>>(path: P) -> Result<Self,Error>{
        let mut r = Reader::from_file(path)?;
        r.trim_text(true);
        r.expand_empty_elements(true);
        Ok(Read{
            reader: r,
            peek: None,
        })
    }
}

pub struct Value<'a>(Event<'a>);

impl<'a> Value<'a>{
    pub fn to_string<R: BufRead>(self,read: &Read<R>) -> Result<String,Error>{
        match self.0 {
            Event::Text(x) => Ok(read.decode(&*x).to_string()),
            x => Err(Error::UnexpectedToken("value".to_string(),format!("{:?}",x)))
        }
    }


    pub fn parse<F,R>(self,read: &Read<R>) -> Result<F,Error>
    where R: BufRead,
          F: FromStr,
          Error: From<F::Err>
    {
        match self.0 {
            Event::Text(x) => Ok(read.decode(&*x).parse()?),
            x => Err(Error::UnexpectedToken("value".to_string(),format!("{:?}",x)))
        }
    }

    pub fn is_start_name<R: BufRead>(&self,name: &str,read: &Read<R>) -> bool{
        match self.0{
            Event::Start(ref x) => {
                let r = read.decode(&*x);
                //trace!("start name: {:?}",r);
                r == name

            },
            _ => false
        }
    }

    pub fn is_end_name<R: BufRead>(&self,name: &str,read: &Read<R>) -> bool{
        match self.0{
            Event::End(ref x) => read.decode(&*x) == name,
            _ => false
        }
    }

    pub fn ok_start_name<R: BufRead>(&self,name: &str,read: &Read<R>) -> Result<(),Error>{
        match self.0{
            Event::Start(ref x) => if read.decode(&*x) == name{
                Ok(())
            }else{
                Err(Error::UnexpectedToken(format!("start token :{}",name),format!("Event::Start({:?})",x)))
            },
            ref x => Err(Error::UnexpectedToken(format!("start token :{}",name),format!("{:?}",x)))
        }
    }

    pub fn ok_end_name<R: BufRead>(&self,name: &str,read: &Read<R>) -> Result<(),Error>{
        match self.0{
            Event::End(ref x) => if read.decode(&*x) == name{
                Ok(())
            }else{
                Err(Error::UnexpectedToken(format!("end token :{}",name),format!("Event::End({:?})",x)))
            },
            ref x => Err(Error::UnexpectedToken(format!("end token :{}",name),format!("{:?}",x)))
        }
    }

}

impl<R: BufRead> Read<R>{
    pub fn next<'a>(&mut self,buf: &'a mut Vec<u8>) -> Result<Value<'a>,Error>{
        if let Some(x) = self.peek.take(){
            Ok(x)
        }else{
            Ok(Value(self.reader.read_event(buf)?))
        }
    }

    pub fn peek(&mut self,buf: &mut Vec<u8>) -> Result<(),Error>{
        if self.peek.is_none(){
            self.peek = Some(Value(self.reader.read_event(buf)?.into_owned()))
        }
        Ok(())
    }

    pub fn get_peek(&self) -> &Value<'static>{
        self.peek.as_ref().unwrap()
    }

    pub fn decode<'b>(&self,d: &'b[u8]) -> Cow<'b,str>{
        self.reader.decode(d)
    }

    pub fn from_token<F>(&mut self,name: &str,buf: &mut Vec<u8>) -> Result<F,Error>
    where F: FromStr,
          Error: From<F::Err>,
    {
        trace!("buf pos: {}",self.buf_pos());
        self.next(buf)?.ok_start_name(name,self)?;
        let res = self.next(buf)?.parse::<F,_>(self)?;
        self.next(buf)?.ok_end_name(name,self)?;
        Ok(res)
    }

    pub fn try_from_token<F>(&mut self,name: &str,buf: &mut Vec<u8>) -> Result<Option<F>,Error>
    where F: FromStr,
          Error: From<F::Err>,
    {
        trace!("buf pos: {}",self.buf_pos());
        self.peek(buf)?;
        if self.get_peek().is_start_name(name,self){
            self.next(buf)?;
            let res = self.next(buf)?.parse::<F,_>(self)?;
            self.next(buf)?.ok_end_name(name,self)?;
            Ok(Some(res))
        }else{
            Ok(None)
        }
    }

    pub fn from_token_list<P>(&mut self,name: &str,buf: &mut Vec<u8>) -> Result<Vec<P>,Error>
    where P: Parse<R>,
    {
        trace!("buf pos: {}",self.buf_pos());
        let mut res = Vec::new();
        self.peek(buf)?;
        while self.get_peek().is_start_name(name,self){
            self.next(buf)?;
            res.push(P::parse(self,buf)?);
            self.next(buf)?.ok_end_name(name,self)?;
            self.peek(buf)?;
        }
        Ok(res)
    }

    pub fn parse_array<P>(&mut self,name_single: &str,name_multiple: &str, buf: &mut Vec<u8>) -> Result<Vec<P>,Error>
    where P: Parse<R>,
    {
        let mut array = Vec::new();
        self.next(buf)?.ok_start_name(name_multiple,self)?;
        self.peek(buf)?;
        let mut cont = self.get_peek().is_start_name(name_single,self);
        while cont {
            self.next(buf)?;
            let item = P::parse(self,buf)?;
            array.push(item);
            self.peek(buf)?;
            cont = self.get_peek().is_start_name(name_single,self)
        }
        self.next(buf)?.ok_end_name(name_multiple,self)?;
        Ok(array)
    }

    pub fn buf_pos(&mut self) -> usize{
        self.reader.buffer_position()
    }
}
