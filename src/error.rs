use quick_xml::Error as XmlError;
use std::{error, num, str, string,fmt};

quick_error!{
    #[derive(Debug)]
    pub enum Error{
        Nested { cur: Box<error::Error>,prev: Box<error::Error> }{
            from(err: (Self,Self)) -> {cur: Box::new(err.0), prev: Box::new(err.1)}
            cause(&**prev)
            display("{}\n@ {}",cur,prev)
        }
        UnexpectedToken(token: String, found: String) {
            display("expected token '{}' found '{}'",token,found)
        }
        EndEarly {
            display("ended to early")
        }
        Custom(field: String) {
            from()
            from(s: &str) -> (s.to_string())
            display("custom: {}",field)
        }
        Xml(err: XmlError) {
            from()
            display("xml error: {}",err)
        }
        ParseString(err: string::ParseError){
            from()
            cause(err)
            display("parse string: {}",err)
        }
        ParseInt(err: num::ParseIntError) {
            from()
            cause(err)
            display("parse int: {}",err)
        }
        ParseFloat(err: num::ParseFloatError) {
            from()
            cause(err)
            display("parse float: {}",err)
        }
        ParseBool(err: str::ParseBoolError) {
            from()
            cause(err)
            display("parse bool: {}",err)
        }
        FromUtf8(err: str::Utf8Error){
            from()
            from(err: string::FromUtf8Error) -> (err.utf8_error())
            cause(err)
            display("Utf8 error: {}",err)
        }
    }
}

impl serde::de::Error for Error{
    fn custom<T>(msg: T) -> Self
        where T: fmt::Display
    {
        Error::Custom(format!("{}",msg))
    }
}
