
use super::prelude::*;

use std::str::FromStr;

#[derive(Debug,Default,Deserialize,GraphQLObject)]
pub struct Coords{
    pub x: i32,
    pub y: i32,
}

impl FromStr for Coords{
    type Err = Error;
    fn from_str(s: &str) -> Result<Self,Error>{
        if let Some(loc) = s.find(','){
            let (x,y) = s.split_at(loc);
            Ok(Coords{
                x: x.parse()?,
                y: y[1..].parse()?,
            })
        }else{
            Err(Error::UnexpectedToken("coordinate".to_string(),s.to_string()))
        }
    }
}

#[derive(Debug,Default,Deserialize,GraphQLObject)]
pub struct Rectangle{
    pub top_left: Coords,
    pub bottom_right: Coords,
}

impl FromStr for Rectangle{
    type Err = Error;
    fn from_str(s: &str) -> Result<Self,Error>{
        if let Some(loc) = s.find(':'){
            let (x,y) = s.split_at(loc);
            Ok(Rectangle{
                top_left: x.parse()?,
                bottom_right: y[1..].parse()?,
            })
        }else{
            Err(Error::UnexpectedToken("rectangle".to_string(),s.to_string()))
        }
    }
}

#[derive(Debug,Default,Deserialize,GraphQLObject)]
pub struct Structure{
    pub local_id: i32,
    pub ty: String,
    pub name: String,
}

impl<R: BufRead> Parse<R> for Structure{
    fn parse(r: &mut Read<R>, buf: &mut Vec<u8>) -> Result<Self,Error>{
        let local_id = r.from_token("local_id",buf)?;
        let ty = r.from_token("type",buf)?;
        let name = r.from_token("name",buf)?;
        r.next(buf)?.ok_end_name("structure",r)?;
        Ok(Structure{
            local_id,
            ty,
            name,
        })
    }
}

//TODO: Structures
#[derive(Debug,Default,Deserialize,GraphQLObject)]
pub struct Site{
    pub id: i32,
    pub ty: String,
    pub name: String,
    pub coords: Coords,
    pub rectangle: Rectangle,
    pub structures: Vec<Structure>,
}

impl<R: BufRead> Parse<R> for Site{
    fn parse(r: &mut Read<R>, buf: &mut Vec<u8>) -> Result<Self,Error>{
        let id = r.from_token::<i32>("id",buf)? - 1;
        let ty = r.from_token("type",buf)?;
        let name = r.from_token("name",buf)?;
        let coords = r.from_token("coords",buf)?;
        let rectangle = r.from_token("rectangle",buf)?;
        r.peek(buf)?;
        let structures = if r.get_peek().is_start_name("structures",r) {
            r.parse_array::<Structure>("structure","structures",buf)?
        }else{
            Vec::new()
        };
        r.next(buf)?.ok_end_name("site",r)?;
        Ok(Site{
            id,
            ty,
            name,
            coords,
            rectangle,
            structures,
        })
    }
}
