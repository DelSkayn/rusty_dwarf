
use super::prelude::*;

#[derive(Debug,Default,Deserialize,GraphQLObject)]
pub struct Artifact{
    pub id: i32,
    pub name: Option<String>,
    pub item: String,
}

impl<R: BufRead> Parse<R> for Artifact{
    fn parse(r: &mut Read<R>, buf: &mut Vec<u8>) -> Result<Self,Error>{
        let id = r.from_token("id",buf)?;
        let name = r.try_from_token("name",buf)?;
        let item = r.from_token("item",buf)?;
        r.next(buf)?.ok_end_name("artifact",r)?;
        Ok(Artifact{
            id,
            name,
            item,
        })
    }
}
