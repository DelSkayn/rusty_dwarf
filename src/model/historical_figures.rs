
use super::prelude::*;


#[derive(Debug,Default,Deserialize,GraphQLObject)]
pub struct RelationShip{
    pub hf_id: i32,
    pub meet_count: i32,
    pub last_meet_year: i32,
    pub last_meet_seconds72: i32,
    pub rep_buddy: Option<i32>,
    pub rep_friendly: Option<i32>,
}

impl<R: BufRead> Parse<R> for RelationShip{
    fn parse(r: &mut Read<R>, buf: &mut Vec<u8>) -> Result<Self,Error>{
        let hf_id = r.from_token("hf_id",buf)?;
        let meet_count = r.from_token("meet_count",buf)?;
        let last_meet_year = r.from_token("last_meet_year",buf)?;
        let last_meet_seconds72 = r.from_token("last_meet_seconds72",buf)?;
        let rep_buddy = r.try_from_token("rep_buddy",buf)?;
        let rep_friendly = r.try_from_token("rep_friendly",buf)?;
        Ok(RelationShip{
            hf_id,
            meet_count,
            last_meet_year,
            last_meet_seconds72,
            rep_buddy,
            rep_friendly,
        })
    }
}

#[derive(Debug,Default,Deserialize,GraphQLObject)]
pub struct EntityReputation{
    pub entity_id: i32,
    pub unsolved_murders: Option<i32>,
    pub first_ageless_year: Option<i32>,
    pub first_ageless_season_count: Option<i32>,
}

impl<R: BufRead> Parse<R> for EntityReputation{
    fn parse(r: &mut Read<R>, buf: &mut Vec<u8>) -> Result<Self,Error>{
        let entity_id = r.from_token("entity_id",buf)?;
        let unsolved_murders = r.try_from_token("unsolved_murders",buf)?;
        let first_ageless_year = r.try_from_token("first_ageless_year",buf)?;
        let first_ageless_season_count = r.try_from_token("first_ageless_season_count",buf)?;
        Ok(EntityReputation{
            entity_id,
            first_ageless_year,
            first_ageless_season_count,
            unsolved_murders,
        })
    }
}

#[derive(Debug,Default,Deserialize,GraphQLObject)]
pub struct HfLink{
    pub ty: String,
    pub hf_id: i32,
    pub link_strength: Option<i32>,
}

impl<R: BufRead> Parse<R> for HfLink{
    fn parse(r: &mut Read<R>, buf: &mut Vec<u8>) -> Result<Self,Error>{
        let ty = r.from_token("link_type",buf)?;
        let hf_id = r.from_token("hfid",buf)?;
        let link_strength = r.try_from_token("link_strength",buf)?;
        Ok(HfLink{
            ty,
            hf_id,
            link_strength,
        })
    }
}

#[derive(Debug,Default,Deserialize,GraphQLObject)]
pub struct HfSkill{
    pub skill: String,
    pub total_ip: i32,
}

impl<R: BufRead> Parse<R> for HfSkill{
    fn parse(r: &mut Read<R>, buf: &mut Vec<u8>) -> Result<Self,Error>{
        let skill = r.from_token("skill",buf)?;
        let total_ip = r.from_token("total_ip",buf)?;
        Ok(HfSkill{
            skill,
            total_ip
        })
    }
}

#[derive(Debug,Default,Deserialize,GraphQLObject)]
pub struct EntityLink{
    pub ty: String,
    pub entity_id: i32,
    pub link_strength: Option<i32>,
}

impl<R: BufRead> Parse<R> for EntityLink{
    fn parse(r: &mut Read<R>, buf: &mut Vec<u8>) -> Result<Self,Error>{
        let ty = r.from_token("link_type",buf)?;
        let entity_id = r.from_token("entity_id",buf)?;
        let link_strength = r.try_from_token("link_strength",buf)?;
        Ok(EntityLink{
            ty,
            entity_id,
            link_strength,
        })
    }
}

#[derive(Debug,Default,Deserialize,GraphQLObject)]
pub struct EntityPositionLink{
    position_profile_id: i32,
    entity_id: i32,
    start_year: Option<i32>,
    end_year: Option<i32>,
}

impl<R: BufRead> Parse<R> for EntityPositionLink{
    fn parse(r: &mut Read<R>, buf: &mut Vec<u8>) -> Result<Self,Error>{
        let position_profile_id = r.from_token("position_profile_id",buf)?;
        let entity_id = r.from_token("entity_id",buf)?;
        let start_year = r.try_from_token("start_year",buf)?;
        let end_year = r.try_from_token("end_year",buf)?;
        Ok(EntityPositionLink{
            position_profile_id,
            entity_id,
            start_year,
            end_year,
        })
    }
}

#[derive(Debug,Default,Deserialize,GraphQLObject)]
pub struct EntitySquadLink{
    squad_id: i32,
    squad_position: Option<i32>,
    entity_id: i32,
    start_year: i32,
    end_year: Option<i32>,
}

impl<R: BufRead> Parse<R> for EntitySquadLink{
    fn parse(r: &mut Read<R>, buf: &mut Vec<u8>) -> Result<Self,Error>{
        let squad_id = r.from_token("squad_id",buf)?;
        let squad_position = r.try_from_token("squad_position",buf)?;
        let entity_id = r.from_token("entity_id",buf)?;
        let start_year = r.from_token("start_year",buf)?;
        let end_year = r.try_from_token("end_year",buf)?;
        Ok(EntitySquadLink{
            squad_id,
            squad_position,
            entity_id,
            start_year,
            end_year,
        })
    }
}

#[derive(Debug,Default,Deserialize,GraphQLObject)]
pub struct SiteLink{
    ty: String,
    occupation_id: Option<i32>,
    site_id: i32,
    sub_id: Option<i32>,
    entity_id: Option<i32>,
}

impl<R: BufRead> Parse<R> for SiteLink{
    fn parse(r: &mut Read<R>, buf: &mut Vec<u8>) -> Result<Self,Error>{
        let ty = r.from_token("link_type",buf)?;
        let occupation_id = r.try_from_token("occupation_id",buf)?;
        let site_id = r.from_token("site_id",buf)?;
        let sub_id = r.try_from_token("sub_id",buf)?;
        let entity_id = r.try_from_token("entity_id",buf)?;
        Ok(SiteLink{
            ty,
            occupation_id,
            site_id,
            sub_id,
            entity_id
        })
    }
}


#[derive(Debug,Default,Deserialize,GraphQLObject)]
pub struct HistoricalFigure{
    pub id: i32,
    pub name: Option<String>,
    pub race: Option<String>,
    pub caste: Option<String>,
    pub appeared: i32,
    pub birth_year: i32,
    pub birth_seconds: i32,
    pub death_year: i32,
    pub death_seconds: i32,
    pub is_deity: bool,
    pub is_force: bool,
    pub is_animated: bool,
    pub associated_type: Option<String>,
    pub hf_links: Vec<HfLink>,
    pub entity_links: Vec<EntityLink>,
    pub entity_position_links: Vec<EntityPositionLink>,
    pub site_links: Vec<SiteLink>,
    pub entity_squad_links: Vec<EntitySquadLink>,
    pub hf_skill: Vec<HfSkill>,
    pub journey_pet: Vec<String>,
    pub knowledge: Vec<String>,
    pub active_interaction: Vec<String>,
    pub animated_string: Option<String>,
    pub entity_reputation: Vec<EntityReputation>,
    pub used_identity_id: Option<i32>,
    pub goals: Vec<String>,
    pub holds_artifact: Vec<i32>,
    pub relationships: Vec<RelationShip>,
    pub ent_pop_id: Option<i32>,
    pub spheres: Vec<String>,
}

impl<R: BufRead> Parse<R> for HistoricalFigure{
    fn parse(r: &mut Read<R>, buf: &mut Vec<u8>) -> Result<Self,Error>{
        let id = r.from_token("id",buf)?;
        let name = r.try_from_token("name",buf)?;
        let race = r.try_from_token("race",buf)?;
        let caste = r.try_from_token("caste",buf)?;

        let appeared = r.from_token("appeared",buf)?;
        let birth_year = r.from_token("birth_year",buf)?;
        let birth_seconds = r.from_token("birth_seconds72",buf)?;
        let death_year = r.from_token("death_year",buf)?;
        let death_seconds = r.from_token("death_seconds72",buf)?;

        r.peek(buf)?;
        let is_deity = if r.get_peek().is_start_name("deity",r){
            r.next(buf)?;
            r.next(buf)?.ok_end_name("deity",r)?;
            true
        }else{
            false
        };

        r.peek(buf)?;
        let is_force = if r.get_peek().is_start_name("force",r){
            r.next(buf)?;
            r.next(buf)?.ok_end_name("force",r)?;
            true
        }else{
            false
        };

        r.peek(buf)?;
        let is_animated = if r.get_peek().is_start_name("animated",r){
            r.next(buf)?;
            r.next(buf)?.ok_end_name("animated",r)?;
            true
        }else{
            false
        };

        let associated_type = r.try_from_token("associated_type",buf)?;

        let hf_links = r.from_token_list("hf_link",buf)?;

        let mut entity_links = Vec::new();
        let mut entity_position_links = Vec::new();
        let mut entity_squad_links = Vec::new();
        r.peek(buf)?;

        loop{
            if r.get_peek().is_start_name("entity_link",r){
                r.next(buf)?;
                entity_links.push(EntityLink::parse(r,buf)?);
                r.next(buf)?.ok_end_name("entity_link",r)?;
                r.peek(buf)?;
            }else if r.get_peek().is_start_name("entity_former_position_link",r){
                r.next(buf)?;
                entity_position_links.push(EntityPositionLink::parse(r,buf)?);
                r.next(buf)?.ok_end_name("entity_former_position_link",r)?;
                r.peek(buf)?;
            }else if r.get_peek().is_start_name("entity_position_link",r){
                r.next(buf)?;
                entity_position_links.push(EntityPositionLink::parse(r,buf)?);
                r.next(buf)?.ok_end_name("entity_position_link",r)?;
                r.peek(buf)?;
            } else if r.get_peek().is_start_name("entity_former_squad_link",r){
                r.next(buf)?;
                entity_squad_links.push(EntitySquadLink::parse(r,buf)?);
                r.next(buf)?.ok_end_name("entity_former_squad_link",r)?;
                r.peek(buf)?;
            }else if r.get_peek().is_start_name("entity_squad_link",r){
                r.next(buf)?;
                entity_squad_links.push(EntitySquadLink::parse(r,buf)?);
                r.next(buf)?.ok_end_name("entity_squad_link",r)?;
                r.peek(buf)?;
            }else{
                break;
            }
        }


        let site_links = r.from_token_list("site_link",buf)?;
        let mut spheres = Vec::new();
        while let Some(x) = r.try_from_token("sphere",buf)?{
            spheres.push(x);
        }

        let hf_skill = r.from_token_list("hf_skill",buf)?;

        let mut journey_pet = Vec::new();
        while let Some(x) = r.try_from_token("journey_pet",buf)?{
            journey_pet.push(x);
        }

        let mut goals = Vec::new();
        while let Some(x) = r.try_from_token("goal",buf)?{
            goals.push(x);
        }

        let mut knowledge = Vec::new();
        while let Some(x) = r.try_from_token("interaction_knowledge",buf)?{
            knowledge.push(x);
        }

        let mut active_interaction = Vec::new();
        while let Some(x) = r.try_from_token("active_interaction",buf)?{
            active_interaction.push(x);
        }

        let animated_string = r.try_from_token("animated_string",buf)?;

        let used_identity_id = r.try_from_token("used_identity_id",buf)?;

        let mut holds_artifact = Vec::new();
        while let Some(x) = r.try_from_token("holds_artifact",buf)?{
            holds_artifact.push(x);
        }

        let entity_reputation = r.from_token_list("entity_reputation",buf)?;

        let relationships = r.from_token_list("relationship_profile_hf",buf)?;

        let ent_pop_id = r.try_from_token("ent_pop_id",buf)?;
        debug!("buf pos: {:?}", r.buf_pos());
        r.next(buf)?.ok_end_name("historical_figure",r)?;

        Ok(HistoricalFigure{
            id,
            name,
            race,
            caste,
            appeared,
            birth_year,
            birth_seconds,
            death_year,
            death_seconds,
            is_deity,
            is_force,
            is_animated,
            associated_type,
            hf_links,
            entity_links,
            entity_position_links,
            entity_squad_links,
            hf_skill,
            journey_pet,
            goals,
            knowledge,
            active_interaction,
            animated_string,
            used_identity_id,
            holds_artifact,
            entity_reputation,
            relationships,
            ent_pop_id,
            site_links,
            spheres,
        })
    }
}
