
use super::prelude::*;

#[derive(Debug,Default,Deserialize,GraphQLObject)]
pub struct Region {
    pub id: i32,
    pub name: String,
    pub ty: String,
}
impl<R: BufRead> Parse<R> for Region{
    fn parse(r: &mut Read<R>, buf: &mut Vec<u8>) -> Result<Self,Error>{
        let id = r.from_token("id",buf)?;
        let name = r.from_token("name",buf)?;
        let ty = r.from_token("type",buf)?;
        r.next(buf)?.ok_end_name("region",r)?;
        Ok(Region{
            id,
            name,
            ty,
        })
    }
}

#[derive(Debug,Default,Deserialize,GraphQLObject)]
pub struct UndergroundRegion {
    pub id: i32,
    pub ty: String,
    pub depth: i32,
}


impl<R: BufRead> Parse<R> for UndergroundRegion{
    fn parse(r: &mut Read<R>, buf: &mut Vec<u8>) -> Result<Self,Error>{
        let id = r.from_token("id",buf)?;
        let ty = r.from_token("type",buf)?;
        let depth = r.from_token("depth",buf)?;
        r.next(buf)?.ok_end_name("underground_region",r)?;
        Ok(UndergroundRegion{
            id,
            ty,
            depth,
        })
    }
}
