
mod prelude{
    pub use crate::read::Read;
    pub use crate::Parse;
    pub use crate::Error;
    pub use std::io::BufRead;
}

mod artifact;
pub use self::artifact::Artifact;

mod region;
pub use self::region::{Region,UndergroundRegion};

mod site;
pub use self::site::{Site,Rectangle,Coords};

mod historical_figures;
pub use self::historical_figures::*;

use self::prelude::*;

#[derive(Debug,Deserialize)]
#[serde(rename = "df_world")]
pub struct DfWorld {
    pub regions: Vec<Region>,
    pub underground_regions: Vec<UndergroundRegion>,
    pub sites: Vec<Site>,
    //pub world_constructions: Vec<()>,
    pub artifacts: Vec<Artifact>,
    pub historical_figures: Vec<HistoricalFigure>,
}

graphql_object!(DfWorld: () |&self|{
    field regions() -> &[Region]{
        &self.regions
    }

    field underground_regions() -> &[UndergroundRegion]{
        &self.underground_regions
    }

    field sites() -> &[Site]{
        &self.sites
    }

    field historical_figures(first: Option<i32>, after: Option<i32>) -> &[HistoricalFigure]{
        let first = first.unwrap_or(10) as usize;
        let after = after.unwrap_or(0) as usize;

        let start = if after > self.historical_figures.len() {
            self.historical_figures.len()
        }else{
            after
        };

        let end = if first + after > self.historical_figures.len() {
            self.historical_figures.len()
        }else{
            first + after
        };

        &self.historical_figures[start..end]
    }

    field region(id: i32) -> Option<&Region>{
        if id < 0 || self.regions.len() < id as usize{
            None
        }else{
            Some(&self.regions[id as usize])
        }
    }

    field underground_region(id: i32) -> Option<&UndergroundRegion>{
        if id < 0 || self.underground_regions.len() < id as usize{
            None
        }else{
            Some(&self.underground_regions[id as usize])
        }
    }

    field site(id: i32) -> Option<&Site>{
        if id < 0|| self.sites.len() < id as usize{
            None
        }else{
            Some(&self.sites[id as usize])
        }
    }

    field historical_figure(id: i32) -> Option<&HistoricalFigure>{
        if id < 0|| self.historical_figures.len() < id as usize{
            None
        }else{
            Some(&self.historical_figures[id as usize])
        }
    }
});

impl Default for DfWorld{
    fn default() -> Self{
        DfWorld{
            regions: vec![Default::default()],
            underground_regions: vec![Default::default()],
            sites: vec![Default::default()],
            artifacts: Vec::new(),
            historical_figures: Vec::new(),
        }
    }

}

impl<R: BufRead> Parse<R> for DfWorld{
    fn parse(r: &mut Read<R>, buf: &mut Vec<u8>) -> Result<Self,Error>{
        let regions = r.parse_array::<Region>("region","regions",buf)?;
        for (i,r) in regions.iter().enumerate(){
            assert_eq!(i,r.id as usize);
        }
        let underground_regions = r.parse_array::<UndergroundRegion>("underground_region","underground_regions",buf)?;
        for (i,r) in underground_regions.iter().enumerate(){
            assert_eq!(i,r.id as usize);
        }
        let sites = r.parse_array::<Site>("site","sites",buf)?;
        for (i,r) in sites.iter().enumerate(){
            assert_eq!(i,r.id as usize);
        }
        r.next(buf)?.ok_start_name("world_constructions",r)?;
        r.next(buf)?.ok_end_name("world_constructions",r)?;

        let artifacts = r.parse_array::<Artifact>("artifact","artifacts",buf)?;
        for (i,r) in sites.iter().enumerate(){
            assert_eq!(i,r.id as usize);
        }

        let historical_figures = r.parse_array::<HistoricalFigure>("historical_figure","historical_figures",buf)?;
        for (i,r) in sites.iter().enumerate(){
            assert_eq!(i,r.id as usize);
        }
        Ok(DfWorld{
            regions,
            underground_regions,
            sites,
            artifacts,
            historical_figures,
        })
    }
}

