extern crate env_logger;
extern crate rusty_dwarf;
#[macro_use]
extern crate log as stdlog;
extern crate juniper;
extern crate juniper_warp;
extern crate warp;

use rusty_dwarf::DfWorld;

use juniper::{EmptyMutation, RootNode};
use warp::{http::Response, log, Filter};

type Schema = RootNode<'static , DfWorld, EmptyMutation<()>>;

use std::env;

fn main() {
    env_logger::init();
    let mut args = env::args();

    let log = log("warp_server");

    let homepage = warp::index().map(|| {
        Response::builder()
            .header("content-type", "text/html")
            .body(format!(
                "<html><h1>juniper_warp</h1><div>visit <a href=\"/graphiql\">/graphiql</a></html>"
            ))
    });
    args.next();
    let path = args.next().unwrap();
    info!("path: {}",path);
    let world = rusty_dwarf::parse_legends(path).unwrap();
    let schema = Schema::new(world,EmptyMutation::new());
    let state = warp::any().map(move ||{
            ()
    });
    let graphql_filter = juniper_warp::make_graphql_filter(schema,state.boxed());

    info!("running server on localhost:8080");
    warp::serve(
        warp::get2()
            .and(warp::path("graphiql"))
            .and(juniper_warp::graphiql_handler("/graphql"))
            .or(homepage)
            .or(warp::path("graphql").and(graphql_filter))
            .with(log),
    ).run(([127, 0, 0, 1], 8080));
}
